from django.db import models
from django.contrib.auth.models import User
from accounts.models import UserPsychologistProfile

#class Question(models.Model):
#	title = models.CharField(max_length=200)
#	body = models.TextField(max_length=5000)
#	user = models.ForeignKey()

class DirectQuestion(models.Model):
	title = models.CharField(max_length=200)
	body = models.TextField(max_length=5000)
	psychologist = models.ForeignKey(UserPsychologistProfile)
	user = models.ForeignKey(User, null=True)

	def __str__(self):
		return self.title

#class DirectScore(models.Model):
#	user = models.ForeignKey(NormalUser)
#	question_score = models.IntegerField(default=0)
#	question = models.ForeignKey(DirectQuestion)
