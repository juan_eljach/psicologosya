import django_filters
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.views.generic import TemplateView
from accounts.models import UserPsychologistProfile

class PsychologistFilter(django_filters.FilterSet):
    class Meta:
        model = UserPsychologistProfile
        fields = ['city']

def product_list(request):
    f = PsychologistFilter(request.GET, queryset=UserPsychologistProfile.objects.all())
    return render_to_response('searches/filter.html', {'filter': f}, context_instance=RequestContext(request))
    #if request.method == "POST":
    #    f = PsychologistFilter(request.POST, queryset=UserPsychologistProfile.objects.all())
    #    context = RequestContext(request)
    #    context["filter"] = f
    #    return render_to_json_response(context)
    #else:
    #    f = PsychologistFilter(request.POST, queryset=UserPsychologistProfile.objects.all())
    #    return render_to_response('searches/template.html', {'filter': f}, context_instance=RequestContext(request))

class LandingPageView(TemplateView):
	template_name = "searches/index.html"

