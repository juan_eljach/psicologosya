from django.conf.urls import patterns, url
from .views import product_list, LandingPageView

urlpatterns = patterns('',
	url(r'^$', LandingPageView.as_view(), name="home"),
    url(r'^list$', product_list, name="list_filter")
)