$(document).ready(function(){

  initSliders();

  //NOTE: To append in different container
  var appendToContainer = function(htmlele, record){
    console.log(record)
  };

  var city = $('#id_city').val();
  
  /*$.ajax({
    method: "POST",
    url: "/",
    data: { city: city }
  })
  .done(function( msg ) {
    //alert( "Data Saved: " + msg );
    var psyco = msg;
  });*/

  //Load Data - example
  var psyco = [
  {
    "name": "Jhon Doe",
    "payment": "tarjeta_credito",
    "precio": 200000,
    "telefono": "234566",
    "therapy": "Pareja"
   },
   {
    "name": "Jhon Dan Doe",
    "payment": "efectivo",
    "precio": 400000,
    "telefono": "123456",
    "therapy": "Familiar"
   },
   {
    "name": "Janet Smith",
    "payment": "consignacion",
    "precio": 100000,
    "telefono": "12345689",
    "therapy": "Individuales"
   },
   {
    "name": "Erika Dan",
    "payment": "efectivo",
    "precio": 80000,
    "telefono": "223344",
    "therapy": "Grupales"
   }
 ];

  var FJS = FilterJS(psyco, '#psyco', {
    template: '#psyco-template',
    //search: {ele: '#searchbox'},
    //search: {ele: '#searchbox', fields: ['runtime']}, // With specific fields
    callbacks: {
      afterFilter: function(result){
        $('#total_psyco').text(result.length);
      }
    }
    //appendToContainer: appendToContainer
  });

  FJS.addCallback('beforeAddRecords', function(){
    if(this.recordsCount >= 450){
      this.stopStreaming();
    }
  });

  FJS.addCallback('afterAddRecords', function(){
    var percent = (this.recordsCount - 250)*100/250;

    $('#stream_progress').text(percent + '%').attr('style', 'width: '+ percent +'%;');

    if (percent == 100){
      $('#stream_progress').parent().fadeOut(1000);
    }
  });

  /*FJS.setStreaming({
    data_url: 'data/stream_movies.json',
    stream_after: 1,
    batch_size: 50
  });*/

  /*FJS.addCriteria({field: 'year', ele: '#year_filter', type: 'range', all: 'all'});
  FJS.addCriteria({field: 'rating', ele: '#rating_filter', type: 'range'});*/
  FJS.addCriteria({field: 'precio', ele: '#rating_filter', type: 'range'});
  FJS.addCriteria({field: 'payment', ele: '#payment input:checkbox'});
  FJS.addCriteria({field: 'therapy', ele: '#therapy input:checkbox'});

  /*
   * Add multiple criterial.
    FJS.addCriteria([
      {field: 'genre', ele: '#genre_criteria input:checkbox'},
      {field: 'year', ele: '#year_filter', type: 'range'}
    ])
  */

  window.FJS = FJS;
});

function initSliders(){
  $("#rating_slider").slider({
    min: 0,
    max: 1000000,
    values:[0, 1000000],
    step: 50000,
    range: true,
    slide: function( event, ui ) {
      $("#rating_range_label" ).html("$" +addCommas(ui.values[ 0 ]) + ' - $' + addCommas(ui.values[ 1 ]));
      $('#rating_filter').val(ui.values[0] + '-' + ui.values[1]).trigger('change');
    }
  });

  
  $('#payment :checkbox').prop('checked', true);
  $('#all_pago').on('click', function(){
    $('#payment :checkbox').prop('checked', $(this).is(':checked'));
  });

  $('#therapy :checkbox').prop('checked', true);
  $('#all_therapy').on('click', function(){
    $('#therapy :checkbox').prop('checked', $(this).is(':checked'));
  });
}


function addCommas(nStr)
{
  nStr += '';
  x = nStr.split('.');
  x1 = x[0];
  x2 = x.length > 1 ? '.' + x[1] : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + '.' + '$2');
  }
  return x1 + x2;
}