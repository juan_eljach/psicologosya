from django.conf.urls import patterns, url
from userena.views import signin
from .views import signup, FORMS, PsychologistOnboardingView, UserCreateView, PsychologistDetail
from .forms import SignupFormExtra

urlpatterns = patterns('',
    url(r'^psicologo/(?P<username>[-\w]+)/$', PsychologistDetail.as_view(), name="psychologist"),
    url(r'^login/$', signin, {"template_name":"accounts/login.html",}, name="login"),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login', name="logout"),
    url(r'^signup/$', signup, {"template_name":"accounts/register.html", 'signup_form': SignupFormExtra}, name="signup"),
    url(r'^users/signup/$', UserCreateView.as_view(), name="signup"),
    url(r'^profiles/create$', PsychologistOnboardingView.as_view(FORMS), name='create_profile'),
)