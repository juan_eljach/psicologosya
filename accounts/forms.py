import random
from django import forms
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _
from userena.forms import SignupForm
from userena.utils import (signin_redirect, get_profile_model, get_user_model,
                           get_user_profile)
from userena.compat import sha_constructor
from django.forms import ModelForm
from .models import UserPsychologistProfile, Degrees, PaymentMethod, Treatment, Therapy, Specialty, PsychologistSpecialties, TreatmentsByPsychologist, PsychologistFocus, PsychologistPayment, PsychologistPromotion, PacientAge
from django.forms.widgets import CheckboxSelectMultiple, Select

class DegreesForm(ModelForm):
#    psychologist = forms.ChoiceField(widget=forms.HiddenInput(), label='')
    class Meta:
        model = Degrees
        fields = ["degree", "university", "year", "years_in_practice"]

class PsychologistSpecialtiesForm(ModelForm):
    class Meta:
        model = PsychologistSpecialties
        fields = ["top_specialty_one", "top_specialty_two", "top_specialty_three", "specialties"]

    def __init__(self, *args, **kwargs):
        super(PsychologistSpecialtiesForm, self).__init__(*args, **kwargs)
        self.fields["specialties"].widget = CheckboxSelectMultiple()
        self.fields["specialties"].queryset = Specialty.objects.all()

class TreatmentsByPsychologistForm(ModelForm):
    class Meta:
        model = TreatmentsByPsychologist
        fields = ["treatments", "therapies"]

    def __init__(self, *args, **kwargs):
        super(TreatmentsByPsychologistForm, self).__init__(*args, **kwargs)
        self.fields["treatments"].widget = CheckboxSelectMultiple()
        self.fields["treatments"].queryset = Treatment.objects.all()
        self.fields["therapies"].widget = CheckboxSelectMultiple()
        self.fields["therapies"].queryset = Therapy.objects.all()

class PsychologistFocusForm(ModelForm):
    class Meta:
        model = PsychologistFocus
        fields = ["pacient_ages", "spoken_languages", "work_on_weekends", "urgencies_service"]

    def __init__(self, *args, **kwargs):
        super(PsychologistFocusForm, self).__init__(*args, **kwargs)
        self.fields["pacient_ages"].widget = CheckboxSelectMultiple()
        self.fields["pacient_ages"].queryset = PacientAge.objects.all()

class PsychologistPaymentForm(ModelForm):
    class Meta:
        model = PsychologistPayment
        fields = ["min_price", "max_price", "free_consultation", "prepaid_medicine", "payment_methods"]

    def __init__(self, *args, **kwargs):
        super(PsychologistPaymentForm, self).__init__(*args, **kwargs)
        self.fields["payment_methods"].widget = CheckboxSelectMultiple()
        self.fields["payment_methods"].queryset = PaymentMethod.objects.all()

class PsychologistPromotionForm(ModelForm):
    class Meta:
        model = PsychologistPromotion
        fields = ["main_description", "second_description", "support_description"]

GENDER_CHOICES = (
	("male","Masculino"),
	("female","Femenino"),
	("other","Otro"),
)

class SignupFormExtra(SignupForm):
    name = forms.CharField(max_length=100, required=False)
    last_name = forms.CharField(max_length=100, required=False)
    gender = forms.ChoiceField(choices=GENDER_CHOICES)
    image = forms.ImageField(required=False)
    country = forms.CharField(max_length=100,required=False)
    state = forms.CharField(max_length=100,required=False)
    city = forms.CharField(max_length=100,required=False)
    address = forms.CharField(max_length=200,required=False)
    phone_number = forms.CharField(max_length=30,required=False)
    institution_name = forms.CharField(max_length=100,required=False)

    def __init__(self, *args, **kwargs):
        super(SignupFormExtra, self).__init__(*args, **kwargs)
        del self.fields['username']

    def save(self):
        """ Generate a random username before falling back to parent signup form """
        full_name = "{0} {1}".format(self.cleaned_data['name'], self.cleaned_data['last_name'])
        username = slugify(full_name)
        while True:
            try:
                user_exists = get_user_model().objects.get(username__iexact=username)
                if user_exists:
                    username = "{0}-{1}".format(username, sha_constructor(str(random.random()).encode('utf-8')).hexdigest()[:5])
            except get_user_model().DoesNotExist: break

        self.cleaned_data['username'] = username

        user = super(SignupFormExtra, self).save()

        user_profile = get_user_profile(user=user)

        user.first_name = self.cleaned_data['name']
        user.last_name = self.cleaned_data['last_name']
        user_profile.name = self.cleaned_data['name']
        user_profile.last_name = self.cleaned_data['last_name']
        user_profile.gender = self.cleaned_data['gender']
        user_profile.image = self.cleaned_data['image']
        user_profile.country = self.cleaned_data['country']
        user_profile.state = self.cleaned_data['state']
        user_profile.city = self.cleaned_data['city']
        user_profile.address = self.cleaned_data['address']
        user_profile.phone_number = self.cleaned_data['phone_number']
        user_profile.institution_name = self.cleaned_data['institution_name']

        user.save()
        user_profile.save()
        return user