# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import easy_thumbnails.fields
import userena.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Degrees',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('degree', models.CharField(max_length=100, choices=[('universidad', 'Diploma Universitario'), ('postgrado', 'Postgrado'), ('certificado', 'Certificado'), ('licencia', 'Licencia'), ('membresia', 'Membresia'), ('reconocimiento', 'Reconocimiento')])),
                ('university', models.CharField(max_length=100)),
                ('year', models.IntegerField()),
                ('years_in_practice', models.IntegerField(choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10), (11, 11), (12, 12), (13, 13), (14, 14), (15, 15), (16, 16), (17, 17), (18, 18), (19, 19), (20, 20)])),
            ],
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('language', models.CharField(max_length=50, choices=[('es', 'Español'), ('en', 'Inglés'), ('fr', 'Francés'), ('al', 'Alemán'), ('it', 'Italiano'), ('pr', 'Portugues'), ('otther', 'Otros')])),
            ],
        ),
        migrations.CreateModel(
            name='PacientAge',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('age', models.CharField(max_length=200, choices=[('0-6', 'Bebes y Niños pequeños (0 a 6 años)'), ('6-10', 'Niños (6 a 10 años)'), ('11-13', 'Preadolescentes (11 a 13 años)'), ('14-18', 'Jóvenes Adolescentes (14 a 18 años'), ('adultos', 'Adultos'), ('+65', 'Adultos Mayores (+65 años)')])),
            ],
        ),
        migrations.CreateModel(
            name='PaymentMethod',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('payment_method', models.CharField(max_length=50, choices=[('cash', 'Dinero en Efectivo'), ('wire', 'Transferencia Bancaria'), ('card', 'Tarjeta de crédito/débito')])),
            ],
        ),
        migrations.CreateModel(
            name='PsychologistFocus',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('work_on_weekends', models.BooleanField(default=False)),
                ('urgencies_service', models.BooleanField(default=False)),
                ('pacient_ages', models.ManyToManyField(to='accounts.PacientAge')),
            ],
        ),
        migrations.CreateModel(
            name='PsychologistPayment',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('min_price', models.CharField(max_length=50)),
                ('max_price', models.CharField(max_length=50)),
                ('free_consultation', models.BooleanField(default=False)),
                ('prepaid_medicine', models.BooleanField(default=False)),
                ('payment_methods', models.ManyToManyField(to='accounts.PaymentMethod')),
            ],
        ),
        migrations.CreateModel(
            name='PsychologistPromotion',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('main_description', models.TextField(max_length=1200)),
                ('second_description', models.TextField(max_length=600)),
                ('support_description', models.TextField(max_length=600)),
            ],
        ),
        migrations.CreateModel(
            name='PsychologistSpecialties',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Specialty',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Therapy',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Treatment',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='TreatmentsByPsychologist',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='UserPsychologistProfile',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('mugshot', easy_thumbnails.fields.ThumbnailerImageField(help_text='A personal image displayed in your profile.', blank=True, verbose_name='mugshot', upload_to=userena.models.upload_to_mugshot)),
                ('privacy', models.CharField(help_text='Designates who can view your profile.', max_length=15, verbose_name='privacy', default='registered', choices=[('open', 'Open'), ('registered', 'Registered'), ('closed', 'Closed')])),
                ('address', models.CharField(max_length=100, blank=True)),
                ('city', models.CharField(max_length=100, blank=True)),
                ('country', models.CharField(max_length=100, blank=True)),
                ('image', models.ImageField(blank=True, upload_to='pictures')),
                ('gender', models.CharField(max_length=20, choices=[('male', 'Masculino'), ('female', 'Femenino'), ('other', 'Otro')])),
                ('license_number', models.IntegerField()),
                ('institution_name', models.CharField(max_length=100, blank=True)),
                ('last_name', models.CharField(max_length=100, blank=True)),
                ('name', models.CharField(max_length=100, blank=True)),
                ('phone_number', models.CharField(max_length=100, blank=True)),
                ('state', models.CharField(max_length=100, blank=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
                'permissions': (('view_profile', 'Can view profile'),),
            },
        ),
        migrations.AddField(
            model_name='treatmentsbypsychologist',
            name='psychologist',
            field=models.ForeignKey(to='accounts.UserPsychologistProfile'),
        ),
        migrations.AddField(
            model_name='treatmentsbypsychologist',
            name='therapies',
            field=models.ManyToManyField(to='accounts.Therapy'),
        ),
        migrations.AddField(
            model_name='treatmentsbypsychologist',
            name='treatments',
            field=models.ManyToManyField(to='accounts.Treatment'),
        ),
        migrations.AddField(
            model_name='psychologistspecialties',
            name='psychologist',
            field=models.ForeignKey(to='accounts.UserPsychologistProfile'),
        ),
        migrations.AddField(
            model_name='psychologistspecialties',
            name='specialties',
            field=models.ManyToManyField(related_name='specialties', to='accounts.Specialty'),
        ),
        migrations.AddField(
            model_name='psychologistspecialties',
            name='top_specialty_one',
            field=models.ForeignKey(to='accounts.Specialty', related_name='specialty_one'),
        ),
        migrations.AddField(
            model_name='psychologistspecialties',
            name='top_specialty_three',
            field=models.ForeignKey(to='accounts.Specialty', related_name='specialty_three'),
        ),
        migrations.AddField(
            model_name='psychologistspecialties',
            name='top_specialty_two',
            field=models.ForeignKey(to='accounts.Specialty', related_name='specialty_two'),
        ),
        migrations.AddField(
            model_name='psychologistpromotion',
            name='psychologist',
            field=models.ForeignKey(to='accounts.UserPsychologistProfile'),
        ),
        migrations.AddField(
            model_name='psychologistpayment',
            name='psychologist',
            field=models.ForeignKey(to='accounts.UserPsychologistProfile'),
        ),
        migrations.AddField(
            model_name='psychologistfocus',
            name='psychologist',
            field=models.ForeignKey(to='accounts.UserPsychologistProfile'),
        ),
        migrations.AddField(
            model_name='psychologistfocus',
            name='spoken_languages',
            field=models.ManyToManyField(to='accounts.Language'),
        ),
        migrations.AddField(
            model_name='degrees',
            name='psychologist',
            field=models.ForeignKey(to='accounts.UserPsychologistProfile'),
        ),
    ]
