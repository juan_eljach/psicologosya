# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_auto_20160208_1913'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userpsychologistprofile',
            name='last_name',
        ),
        migrations.RemoveField(
            model_name='userpsychologistprofile',
            name='name',
        ),
    ]
