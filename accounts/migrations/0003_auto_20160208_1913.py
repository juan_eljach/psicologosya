# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import easy_thumbnails.fields
import userena.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('accounts', '0002_auto_20160124_1946'),
    ]

    operations = [
        migrations.CreateModel(
            name='BaseProfile',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('mugshot', easy_thumbnails.fields.ThumbnailerImageField(upload_to=userena.models.upload_to_mugshot, verbose_name='mugshot', help_text='A personal image displayed in your profile.', blank=True)),
                ('privacy', models.CharField(default='registered', verbose_name='privacy', help_text='Designates who can view your profile.', choices=[('open', 'Open'), ('registered', 'Registered'), ('closed', 'Closed')], max_length=15)),
            ],
            options={
                'abstract': False,
                'permissions': (('view_profile', 'Can view profile'),),
            },
        ),
        migrations.AlterField(
            model_name='therapy',
            name='name',
            field=models.CharField(choices=[('individuales', 'Individuales'), ('grupales', 'En Grupo'), ('familiares', 'Familiares'), ('parejas', 'Pareja')], max_length=200),
        ),
        migrations.CreateModel(
            name='NormalUser',
            fields=[
                ('baseprofile_ptr', models.OneToOneField(serialize=False, auto_created=True, primary_key=True, to='accounts.BaseProfile', parent_link=True)),
            ],
            options={
                'abstract': False,
                'permissions': (('view_profile', 'Can view profile'),),
            },
            bases=('accounts.baseprofile',),
        ),
        migrations.AddField(
            model_name='baseprofile',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
        ),
    ]
