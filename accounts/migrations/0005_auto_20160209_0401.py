# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_auto_20160208_2117'),
    ]

    operations = [
        migrations.AlterField(
            model_name='baseprofile',
            name='privacy',
            field=models.CharField(verbose_name='privacy', default='open', choices=[('open', 'Open'), ('registered', 'Registered'), ('closed', 'Closed')], help_text='Designates who can view your profile.', max_length=15),
        ),
        migrations.AlterField(
            model_name='psychologistspecialties',
            name='psychologist',
            field=models.OneToOneField(to='accounts.UserPsychologistProfile'),
        ),
        migrations.AlterField(
            model_name='userpsychologistprofile',
            name='privacy',
            field=models.CharField(verbose_name='privacy', default='open', choices=[('open', 'Open'), ('registered', 'Registered'), ('closed', 'Closed')], help_text='Designates who can view your profile.', max_length=15),
        ),
    ]
