from django.contrib import admin
from .models import Specialty, Treatment, Therapy, Language, PacientAge, PaymentMethod, PsychologistSpecialties, PsychologistPromotion, PsychologistPayment, PsychologistFocus, TreatmentsByPsychologist

admin.site.register(Specialty)
admin.site.register(Treatment)
admin.site.register(Therapy)
admin.site.register(Language)
admin.site.register(PacientAge)
admin.site.register(PaymentMethod)
admin.site.register(PsychologistSpecialties)
admin.site.register(PsychologistPromotion)
admin.site.register(PsychologistPayment)
admin.site.register(PsychologistFocus)
admin.site.register(TreatmentsByPsychologist)
# Register your models here.
