import warnings
from django.shortcuts import render, render_to_response
from django.core.urlresolvers import reverse
from django.shortcuts import redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout, REDIRECT_FIELD_NAME
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.views.generic import TemplateView, CreateView, DetailView
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext as _
from django.http import Http404, HttpResponseRedirect
from django.forms import formset_factory
from userena.forms import SignupForm
from userena.models import UserenaSignup
from userena.decorators import secure_required
from userena.utils import (signin_redirect, get_profile_model, get_user_model,
                           get_user_profile)
from userena import signals as userena_signals
from userena import settings as userena_settings
from guardian.decorators import permission_required_or_403
from formtools.wizard.views import SessionWizardView
from .forms import DegreesForm, PsychologistSpecialtiesForm, TreatmentsByPsychologistForm, PsychologistFocusForm, PsychologistPaymentForm, PsychologistPromotionForm
from .models import UserPsychologistProfile

class UserCreateView(CreateView):
    model = User
    form_class = UserCreationForm
    template_name = "accounts/user_signup.html"

class PsychologistDetail(DetailView):
    model = UserPsychologistProfile
    template_name = "accounts/profile_detail_for_user.html"
    context_object_name = "profile"

    def get_object(self):
        username = self.kwargs.get("username")
        user = get_user_model().objects.get(username__iexact=username)
        profile = get_user_profile(user)
        return profile


class ExtraContextTemplateView(TemplateView):
    """ Add extra context to a simple template view """
    extra_context = None

    def get_context_data(self, *args, **kwargs):
        context = super(ExtraContextTemplateView, self).get_context_data(*args, **kwargs)
        if self.extra_context:
            context.update(self.extra_context)
        return context

    # this view is used in POST requests, e.g. signup when the form is not valid
    post = TemplateView.get

@secure_required
def signup(request, signup_form=SignupForm,
           template_name='userena/signup_form.html', success_url=None,
           extra_context=None):
    """
    Signup of an account.

    Signup requiring a username, email and password. After signup a user gets
    an email with an activation link used to activate their account. After
    successful signup redirects to ``success_url``.

    :param signup_form:
        Form that will be used to sign a user. Defaults to userena's
        :class:`SignupForm`.

    :param template_name:
        String containing the template name that will be used to display the
        signup form. Defaults to ``userena/signup_form.html``.

    :param success_url:
        String containing the URI which should be redirected to after a
        successful signup. If not supplied will redirect to
        ``userena_signup_complete`` view.

    :param extra_context:
        Dictionary containing variables which are added to the template
        context. Defaults to a dictionary with a ``form`` key containing the
        ``signup_form``.

    **Context**

    ``form``
        Form supplied by ``signup_form``.

    """
    # If signup is disabled, return 403
    if userena_settings.USERENA_DISABLE_SIGNUP:
        raise PermissionDenied

    # If no usernames are wanted and the default form is used, fallback to the
    # default form that doesn't display to enter the username.
    if userena_settings.USERENA_WITHOUT_USERNAMES and (signup_form == SignupForm):
        print ("USERNA SIN USERNAMESSSSSSSSSSSSSSSS")
        signup_form = SignupFormOnlyEmail

    form = signup_form()

    if request.method == 'POST':
        form = signup_form(request.POST, request.FILES)
        if form.is_valid():
            user = form.save()

            # Send the signup complete signal
            userena_signals.signup_complete.send(sender=None,
                                                 user=user)


            if success_url: redirect_to = success_url
            else: redirect_to = reverse('userena_signup_complete',
                                        kwargs={'username': user.username})

            # A new signed user should logout the old one.
            if request.user.is_authenticated():
                logout(request)

            if (userena_settings.USERENA_SIGNIN_AFTER_SIGNUP and
                not userena_settings.USERENA_ACTIVATION_REQUIRED):
                user = authenticate(identification=user.email, check_password=False)
                login(request, user)

            return redirect(redirect_to)

    if not extra_context: extra_context = dict()
    extra_context['form'] = form
    return ExtraContextTemplateView.as_view(template_name=template_name,
                                            extra_context=extra_context)(request)
@secure_required
def activate(request, activation_key,
             template_name='userena/activate_fail.html',
             retry_template_name='userena/activate_retry.html',
             success_url=None, extra_context=None):
    """
    Activate a user with an activation key.

    The key is a SHA1 string. When the SHA1 is found with an
    :class:`UserenaSignup`, the :class:`User` of that account will be
    activated.  After a successful activation the view will redirect to
    ``success_url``.  If the SHA1 is not found, the user will be shown the
    ``template_name`` template displaying a fail message.
    If the SHA1 is found but expired, ``retry_template_name`` is used instead,
    so the user can proceed to :func:`activate_retry` to get a new activation key.

    :param activation_key:
        String of a SHA1 string of 40 characters long. A SHA1 is always 160bit
        long, with 4 bits per character this makes it --160/4-- 40 characters
        long.

    :param template_name:
        String containing the template name that is used when the
        ``activation_key`` is invalid and the activation fails. Defaults to
        ``userena/activate_fail.html``.

    :param retry_template_name:
        String containing the template name that is used when the
        ``activation_key`` is expired. Defaults to
        ``userena/activate_retry.html``.

    :param success_url:
        String containing the URL where the user should be redirected to after
        a successful activation. Will replace ``%(username)s`` with string
        formatting if supplied. If ``success_url`` is left empty, will direct
        to ``userena_profile_detail`` view.

    :param extra_context:
        Dictionary containing variables which could be added to the template
        context. Default to an empty dictionary.

    """
    try:
        if (not UserenaSignup.objects.check_expired_activation(activation_key)
            or not userena_settings.USERENA_ACTIVATION_RETRY):
            user = UserenaSignup.objects.activate_user(activation_key)
            if user:
                # Sign the user in.
                auth_user = authenticate(identification=user.email,
                                         check_password=False)
                login(request, auth_user)

                if userena_settings.USERENA_USE_MESSAGES:
                    messages.success(request, _('Your account has been activated and you have been signed in.'),
                                     fail_silently=True)

                if success_url: redirect_to = success_url % {'username': user.username }
                else: redirect_to = reverse('userena_profile_detail',
                                            kwargs={'username': user.username})
                return redirect(redirect_to)
            else:
                if not extra_context: extra_context = dict()
                return ExtraContextTemplateView.as_view(template_name=template_name,
                                                        extra_context=extra_context)(
                                        request)
        else:
            if not extra_context: extra_context = dict()
            extra_context['activation_key'] = activation_key
            return ExtraContextTemplateView.as_view(template_name=retry_template_name,
                                                extra_context=extra_context)(request)
    except UserenaSignup.DoesNotExist:
        if not extra_context: extra_context = dict()
        return ExtraContextTemplateView.as_view(template_name=template_name,
                                                extra_context=extra_context)(request)

FORMS = [
	("degrees", formset_factory(DegreesForm, extra=3)),
	("specialties", PsychologistSpecialtiesForm),
	("treatments", TreatmentsByPsychologistForm),
	("focus", PsychologistFocusForm),
	("payment", PsychologistPaymentForm),
	("promotion", PsychologistPromotionForm),
]

class PsychologistOnboardingView(SessionWizardView):
    template_name = "accounts/steps_profile.html"
    
    def done(self, form_list, **kwargs):
        user = self.request.user
        user_profile = get_object_or_404(UserPsychologistProfile, user=user)
        for form_step in form_list:
            if hasattr(form_step, "max_num"):
                for form in form_step:
                    form_instance = form.save(commit=False)
                    form_instance.psychologist = user_profile
                    form_instance.save()
                    try:
                        form_step.save_m2m()
                    except Exception as lol:
                        print (lol)
            if not hasattr(form_step, "max_num") and form_step.is_valid():
                form_instance = form_step.save(commit=False)
                form_instance.psychologist = user_profile
                form_instance.save()
                try:
                    form_step.save_m2m()
                except Exception as lol:
                    print (lol)
            else:
                print ("THERE WAS A BIG MOTHER FUCKING ERROR")
        return HttpResponseRedirect("/lol")

    def get_context_data(self, form, **kwargs):
        context = super(PsychologistOnboardingView, self).get_context_data(form=form, **kwargs)
        user = self.request.user
        user_profile = get_object_or_404(UserPsychologistProfile, user=user)
        context["user_profile"] = user_profile
        return context