import django_filters
from django.db import models
from django.contrib.auth.models import User
from userena.models import UserenaBaseProfile

GENDER_CHOICES = (
	("male","Masculino"),
	("female","Femenino"),
	("other","Otro"),
)

class UserProfile(UserenaBaseProfile):
	user = models.OneToOneField(User)

class UserPsychologistProfile(UserenaBaseProfile):
    user = models.OneToOneField(User)
    address = models.CharField(max_length=100, blank=True)
    city = models.CharField(max_length=100, blank=True)
    country = models.CharField(max_length=100, blank=True)
    gender = models.CharField(max_length=20, choices=GENDER_CHOICES)
    license_number = models.IntegerField(null=True)
    institution_name = models.CharField(max_length=100, blank=True)
    phone_number = models.CharField(max_length=100, blank=True)
    state = models.CharField(max_length=100, blank=True)

DEGREES_CHOICES = (
	("universidad","Diploma Universitario"),
	("postgrado","Postgrado"),
	("certificado","Certificado"),
	("licencia","Licencia"),
	("membresia","Membresia"),
	("reconocimiento","Reconocimiento"),
)

YEARS_IN_PRACTICE_CHOICES = [(x,x) for x in range(1,21)]

class Degrees(models.Model):
	psychologist = models.ForeignKey(UserPsychologistProfile)
	degree = models.CharField(max_length=100, choices=DEGREES_CHOICES)
	university = models.CharField(max_length=100)
	year = models.IntegerField()
	years_in_practice = models.IntegerField(choices=YEARS_IN_PRACTICE_CHOICES)

	def __str__(self):
		return self.degree

class Specialty(models.Model):
	name = models.CharField(max_length=200)

	def __str__(self):
		return self.name

class PsychologistSpecialties(models.Model):
	psychologist = models.OneToOneField(UserPsychologistProfile)
	top_specialty_one = models.ForeignKey(Specialty, related_name="specialty_one")
	top_specialty_two = models.ForeignKey(Specialty, related_name="specialty_two")
	top_specialty_three = models.ForeignKey(Specialty, related_name="specialty_three")
	specialties = models.ManyToManyField(Specialty, related_name="specialties")

	def __str__(self):
		return self.psychologist.user.get_full_name()

class Treatment(models.Model):
	name = models.CharField(max_length=200)

	def __str__(self):
		return self.name

THERAPY_CHOICES = (
	("individuales", "Individuales"),
	("grupales", "En Grupo"),
	("familiares", "Familiares"),
	("parejas", "Pareja"),
)

class Therapy(models.Model):
	name = models.CharField(max_length=200, choices=THERAPY_CHOICES)

	def __str__(self):
		return self.name

class TreatmentsByPsychologist(models.Model):
	psychologist = models.ForeignKey(UserPsychologistProfile)
	treatments = models.ManyToManyField(Treatment)
	therapies = models.ManyToManyField(Therapy)

	def __str__(self):
		return self.psychologist.user.get_full_name()

PACIENT_AGES_CHOICES = (
	("0-6", "Bebes y Niños pequeños (0 a 6 años)"),
	("6-10", "Niños (6 a 10 años)"),
	("11-13", "Preadolescentes (11 a 13 años)"),
	("14-18", "Jóvenes Adolescentes (14 a 18 años"),
	("adultos", "Adultos"),
	("+65", "Adultos Mayores (+65 años)"),

)

class PacientAge(models.Model):
	age = models.CharField(max_length=200, choices=PACIENT_AGES_CHOICES)

	def __str__(self):
		return self.age


LANGUAGE_CHOICES = (
	("es", "Español"),
	("en", "Inglés"),
	("fr", "Francés"),
	("al", "Alemán"),
	("it", "Italiano"),
	("pr", "Portugues"),
	("otther", "Otros"),
)

class Language(models.Model):
	language = models.CharField(max_length=50, choices=LANGUAGE_CHOICES)

	def __str__(self):
		return self.language

class PsychologistFocus(models.Model):
	psychologist = models.ForeignKey(UserPsychologistProfile)
	pacient_ages = models.ManyToManyField(PacientAge)
	spoken_languages = models.ManyToManyField(Language)
	work_on_weekends = models.BooleanField(default=False)
	urgencies_service = models.BooleanField(default=False)

	def __str__(self):
		return self.psychologist.user.get_full_name()


PAYMENT_CHOICES = (
	("cash","Dinero en Efectivo"),
	("wire","Transferencia Bancaria"),
	("card","Tarjeta de crédito/débito"),
)

class PaymentMethod(models.Model):
	payment_method = models.CharField(max_length=50, choices=PAYMENT_CHOICES)

	def __str__(self):
		return self.payment_method

class PsychologistPayment(models.Model):
	psychologist = models.OneToOneField(UserPsychologistProfile)
	min_price = models.CharField(max_length=50)
	max_price = models.CharField(max_length=50)
	free_consultation = models.BooleanField(default=False)
	prepaid_medicine = models.BooleanField(default=False)
	payment_methods = models.ManyToManyField(PaymentMethod)

	def __str__(self):
		return self.psychologist.user.get_full_name()

class PsychologistPromotion(models.Model):
	psychologist = models.ForeignKey(UserPsychologistProfile)
	main_description = models.TextField(max_length=1200)
	second_description = models.TextField(max_length=600)
	support_description = models.TextField(max_length=600)

	def __str__(self):
		return self.psychologist.user.get_full_name()
