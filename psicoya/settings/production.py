"""Production settings and globals."""

from __future__ import absolute_import

from os import environ

from .base import *

# Normally you should not import ANYTHING from Django directly
# into your settings, but ImproperlyConfigured is an exception.
from django.core.exceptions import ImproperlyConfigured


#def get_env_setting(setting):
#    """ Get the environment setting or return exception """
#    try:
#        return environ[setting]
#    except KeyError:
#        error_msg = "Set the %s env variable" % setting
#        raise ImproperlyConfigured(error_msg)

########## HOST CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/releases/1.5/#allowed-hosts-required-in-production
#ALLOWED_HOSTS = ['.exploiter.co', '*']
########## END HOST CONFIGURATION

EMAIL_HOST = "smtp.sendgrid.net"
EMAIL_HOST_USER = "psicologosya"
EMAIL_HOST_PASSWORD = "Psicologos123"
EMAIL_PORT = 587
EMAIL_USE_TLS = True

#DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.postgresql_psycopg2',
#        'NAME': "exploiter_db",
#        'USER': "root",
#        'PASSWORD': "w4tW#2wpgB#B71@ZjpSjj_7oc",
#        'HOST': 'localhost',
#        'PORT': '5432',
#    }
#}
########## END DATABASE CONFIGURATION


########## CACHE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#caches
#CACHES = {
#    'default': {
#        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
#        'LOCATION': '/var/tmp/django_cache',
#    }
#}
########## END CACHE CONFIGURATION

GOOGLE_ANALYTICS = True