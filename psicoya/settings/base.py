#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from os.path import abspath, basename, dirname, join, normpath
from sys import path
from django.core.urlresolvers import reverse_lazy

DJANGO_ROOT = dirname(dirname(abspath(__file__)))

SITE_ROOT = dirname(DJANGO_ROOT)

SITE_NAME = basename(DJANGO_ROOT)

path.append(DJANGO_ROOT)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SITE_ID = 1
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'p^khh7urv36w6%9*-*4y+i6j*c_#_i*m4_vs0l#8e$+t4jbj=a'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

DJANGO_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)

LOCAL_APPS = (
    'accounts',
    'questions',
)

THIRD_PARTY_APPS = (
    'django_filters',
    'formtools',
    'userena',
    'guardian',
    'easy_thumbnails',
    'postman',
)

INSTALLED_APPS = DJANGO_APPS + LOCAL_APPS + THIRD_PARTY_APPS

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'psicoya.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [normpath(join(SITE_ROOT, 'templates'))],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'psicoya.context_processors.google_analytics',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'postman.context_processors.inbox',
            ],
        },
    },
]

WSGI_APPLICATION = 'psicoya.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "psicoya",
        "USER": "hood",
        "PASSWORD": "makeshithappen",
        "HOST": "localhost",
        "PORT": "5432",
    }
}


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'es-CO'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_ROOT = normpath(join(SITE_ROOT, 'media'))

MEDIA_URL = '/media/'

STATICFILES_DIRS = (
    normpath(join(SITE_ROOT, 'static')),
    )

STATIC_ROOT = normpath(join(SITE_ROOT, 'assets'))

STATIC_URL = '/static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'dajaxice.finders.DajaxiceFinder',
)


AUTHENTICATION_BACKENDS = (
    'userena.backends.UserenaAuthenticationBackend',
    'guardian.backends.ObjectPermissionBackend',
    'django.contrib.auth.backends.ModelBackend',
)

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = "smtp.sendgrid.net"
EMAIL_HOST_USER = "psicologosya"
EMAIL_HOST_PASSWORD = "Psicologos123"
EMAIL_PORT = 587
EMAIL_USE_TLS = True

DEFAULT_FROM_EMAIL = "Buzón de PsicologosYa! <contacto@psicologosya.com>"

ANONYMOUS_USER_ID = -1

SITE_ID = 2

USERENA_ACTIVATION_REQUIRED = False

USERENA_DEFAULT_PRIVACY = "open"

USERENA_PROFILE_DETAIL_TEMPLATE = "accounts/profile_detail.html"

USERENA_WITHOUT_USERNAMES = True

AUTH_PROFILE_MODULE = 'accounts.UserPsychologistProfile'

POSTMAN_DISALLOW_ANONYMOUS = True

POSTMAN_DISALLOW_MULTIRECIPIENTS = True

POSTMAN_DISALLOW_COPIES_ON_REPLY = True

POSTMAN_AUTO_MODERATE_AS= True